# tarkov-cli

Simple cli interface for Tarkov dev's [API](https://tarkov.dev/api/).

## Windows Installation
1. Make sure you have python 3 [installed](https://www.python.org/downloads/windows/).
    ```shell
    python --version
    ```
    Should return version information.

2. [Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) the [repository](https://gitlab.com/gummigudm/tarkov-cli).
    ```shell
    cd
    mkdir scripts
    cd scripts
    git clone https://gitlab.com/gummigudm/tarkov-cli.git
    ```

3. Make sure you have pip [installed](https://www.geeksforgeeks.org/how-to-install-pip-on-windows/).
    ```shell
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python get-pip.py
    ```

4. Install requests module
    ```shell
    pip install requests
    ```

5. Run the cli
    ```shell
    cd tarkov-cli
    python tarkov item <search term>
    ```

## Usage

Run commands using your python installation.
```shell
python tarkov item <search term>
python tarkov item Duct tape
python C:\Location\tarkov-cli\tarkov item Duct tape
```

For now the cli only supports item search, but tasks and other functionality will be added later.
If a search term covers multiple items, a list will be shown, but filtering down to a single item, renders information about purchasing prices, selling prices and barters.
